package com.app.demo

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.app.demo.databinding.ProgressDialogBinding



class ProgressDialog(private var cancellable: Boolean) : BaseDialogFragment() {

    private lateinit var binding: ProgressDialogBinding

    companion object {

        private var confirmDialog: ProgressDialog? = null

        fun getInstance(cancellable: Boolean): ProgressDialog {
            if (confirmDialog == null) {
                confirmDialog = ProgressDialog(cancellable)
            }
            this.confirmDialog?.cancellable = cancellable
            return confirmDialog as ProgressDialog
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val binding = ProgressDialogBinding.inflate(
                inflater,
                container,
                false
        )

        this.binding = binding

        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        }
        dialog?.setCanceledOnTouchOutside(cancellable)

        return binding.root
    }

    override fun setValues() {
    }

    override fun setListener() {
    }

    /**
     * Used to dismiss the progress dialog
     * */
    fun dismissDialog() {
        if (dialog != null) {
            dismiss()
        }
    }
}