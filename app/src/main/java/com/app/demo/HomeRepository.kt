package com.app.demo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.app.demo.local.DemoDao
import com.app.demo.local.DemoDb
import com.app.demo.network.Item
import com.app.demo.network.Response
import com.app.demo.network.Service
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class HomeRepository @Inject constructor(private val service: Service,private val demoDao: DemoDao ) {


    fun getDataList(): Flow<PagingData<Item>> {
        return Pager(
            config = PagingConfig(
                pageSize = 4,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { PagingDataSource(service,demoDao) }
        ).flow
    }

    suspend fun updateCommentInDb(value: String, id: Int) {
        val data = demoDao.getDataById(id)
        val comments = data.comments?.toMutableList() ?: emptyList<String>().toMutableList()
        comments.add(value)
        demoDao.updateDataById(comments.toList(),id)
    }

    suspend fun getSelectedData(id: Int)= demoDao.getDataById(id)

}