package com.app.demo

import com.app.demo.network.Item
import java.text.FieldPosition

interface ClickListener {
    fun onItemClick(value : String, position: Int)
    fun onPositionClick(position : Item)
}