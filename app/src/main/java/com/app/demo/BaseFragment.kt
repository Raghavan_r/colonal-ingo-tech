package com.app.demo

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : Fragment(){

    private val progressDialog = ProgressDialog.getInstance(false)


    fun showSnackBar(message: String) {
        Snackbar.make(
                requireView(),
                message,
                Snackbar.LENGTH_SHORT
        ).show()
    }

    protected fun showProgress(show: Boolean) {
        if (show) {
            progressDialog.show(childFragmentManager, "Progress")
        } else {
            progressDialog.dismissDialog()
        }
    }

}