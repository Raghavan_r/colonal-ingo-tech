package com.app.demo.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface Service {

    @GET("search/repositories?q=android%20language:java&sort=stars&order=desc&per_page=2")
    suspend fun getUserList(@Query (value = "page") pageSize: Int): Response


    companion object {

        fun createService(
            httpClient: HttpClientBuilderFactory
        ): Service {

            val okHttpClient = httpClient.create().build()

            return Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Service::class.java)
        }
    }
}