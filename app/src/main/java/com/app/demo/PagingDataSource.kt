package com.app.demo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.paging.PagingSource
import com.app.demo.local.DemoDao
import com.app.demo.network.Item
import com.app.demo.network.Response
import com.app.demo.network.Service

class PagingDataSource(
        private val service: Service,
        private val demoDao: DemoDao
) :
        PagingSource<Int, Item>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Item> {
        val position = params.key ?: 1
        return try {
            val response =
                    service.getUserList(position)
            demoDao.insertAll(response.items)
            val repos = response.items

            LoadResult.Page(
                    data = repos,
                    prevKey = if (position == 1) null else position - 1,
                    nextKey = if (repos.isEmpty()) null else position + 1
            )
        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }
}